import os
import argparse
import shlex
import subprocess


def rsync_module(src, dest, delete, doit):
    rsync_cmd = ['rsync', '-r', '--links']
    rsync_cmd += ['--checksum', '--verbose']
    if delete is True:
        rsync_cmd += ['--delete']
    if doit is not True:
        rsync_cmd += ['--dry-run']
    rsync_cmd += [src, dest]
    rsync_cmd = shlex.split(' '.join(map(str, rsync_cmd)))
    print(' '.join(map(str, rsync_cmd)))
    rsync_proc = subprocess.Popen(rsync_cmd)
    rsync_proc.communicate()[0]


def list_source(source, modulename, modulepath, softwarepath):
    paths = {'mod': None, 'soft': None}
    if os.path.isdir(source):
        for item in os.listdir(source):
            if item == modulepath:
                paths['mod'] = os.path.join(source, item, modulename)
            elif item == softwarepath:
                paths['soft'] = os.path.join(source, item, modulename)
    else:
        print('%s not found' % source)
    if os.path.isdir(paths['soft']) == os.path.isdir(paths['soft']) is True:
        return paths
    else:
        print('Paths do not match')


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('modulename',
                        help='Name of the module to install')
    parser.add_argument('-s', '--source',  dest='source',
                        help='Source path of the module to deply',
                        type=str, required=True)
    parser.add_argument('-t', '--target',  dest='target',
                        help='Target folder',
                        type=str, required=True)
    parser.add_argument('-d', '--delete',  dest='delete',
                        help='Delete file not present in the source',
                        action='store_true')
    parser.add_argument('-a', '--apply',  dest='apply',
                        help='Apply changes (default only dry-run)',
                        action='store_true')
    args = parser.parse_args()

    paths = list_source(args.source, args.modulename,
                        'modulefiles', 'software')
    targ_mod = os.path.join(args.target, 'modulefiles')
    targ_soft = os.path.join(args.target, 'software')
    if os.path.isdir(targ_mod) == os.path.isdir(targ_soft) is True:
        print('copy %s to %s' % (paths['mod'], targ_soft))
        rsync_module(paths['mod'], targ_mod, args.delete, args.apply)
        print('copy %s to %s' % (paths['soft'], targ_soft))
        rsync_module(paths['soft'], targ_soft, args.delete, args.apply)

if __name__ == "__main__":
    main()

