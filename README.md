
Handy script to deploy modules from the git repository to an app folder in computerome


```
python inst_mod.py -h
usage: inst_mod.py [-h] -s SOURCE -t TARGET [-d] [-a] modulename

positional arguments:
  modulename            Name of the module to install

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        Source path of the module to deply
  -t TARGET, --target TARGET
                        Target folder
  -d, --delete          Delete file not present in the source
  -a, --apply           Apply changes (default only dry-run)

```



```
python inst_mod.py  -s cov_plot -t /home/projects/cu_10027/apps cov_plot
copy cov_plot/modulefiles/cov_plot to /home/projects/cu_10027/apps/software
rsync -r --links --checksum --verbose --dry-run cov_plot/modulefiles/cov_plot /home/projects/cu_10027/apps/modulefiles
sending incremental file list

sent 104 bytes  received 13 bytes  234.00 bytes/sec   
total size is 1425  speedup is 12.18 (DRY RUN)
copy cov_plot/software/cov_plot to /home/projects/cu_10027/apps/software
rsync -r --links --checksum --verbose --dry-run cov_plot/software/cov_plot /home/projects/cu_10027/apps/software
sending incremental file list
cov_plot/0.0.1/R/read_depth_ratio_sv_bedpe_plusCovSigSeg.R

sent 285 bytes  received 19 bytes  608.00 bytes/sec   
total size is 28217  speedup is 92.82 (DRY RUN)

```
